import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SourceEnum } from 'src/app/shared/enums';
import { DefaultResponse, ScrapingResult } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

const API_URL = `${environment.host}/api/v1/it`;

@Injectable({
  providedIn: 'root',
})
export class ScrapingApiService {
  constructor(private http: HttpClient) {}

  public getNewsFromSource(
    source?: SourceEnum,
    limit?: number,
    offset?: number,
    orderingBy?: 'ASC' | 'DESC',
    risk?: number,
    filterDate?: number
  ): Observable<DefaultResponse<ScrapingResult[]>> {
    let params = new HttpParams();
    if (source && source.toString() != 'ALL') params = params.append('source', source);
    if (!isNaN(offset) && !isNaN(limit)) {
      params = params.append('offset', offset);
      params = params.append('limit', limit);
    }
    if (orderingBy) params = params.append('orderingBy', orderingBy);
    if (!isNaN(risk)) params = params.append('risk', risk);
    if (!isNaN(filterDate)) params = params.append('filterDate', filterDate);

    console.log(params.toString())
    return this.http.get<DefaultResponse<ScrapingResult[]>>(
      `${API_URL}/scraping`,
      { params: params }
    );
  }

  public getRiskSummaryFromSource(
    source?: SourceEnum,
    filterDate?: number
  ): Observable<number[][]> {
    let params = new HttpParams();
    if (source) params = params.append('source', source);
    if (!isNaN(filterDate)) params = params.append('filterDate', filterDate);

    return this.http.get<number[][]>(
      `${API_URL}/risk`,
      { params: params }
    );
  }
}
