import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SessionHelper {
  constructor() {}

  public getItem<T>(key: string): T | null {
    const item = sessionStorage.getItem(key);
    if (item === null)
      return null;
    return JSON.parse(item);
  }

  public saveItem<T>(key: string, item: T): void {
    this.deleteItem(key);
    sessionStorage.setItem(key, JSON.stringify(item));
  }

  public deleteItem(key: string): void {
    sessionStorage.removeItem(key);
  }

  public clearSession(): void {
    sessionStorage.clear();
  }
}
