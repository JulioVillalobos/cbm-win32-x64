import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';

import { Observable } from 'rxjs';
//import { TokenStorageService } from '../../@shared/services/token-storage.service';

// const TOKEN_HEADER_KEY = 'Authorization';       // for Spring Boot back-end
// const TOKEN_HEADER_KEY = 'x-access-token';   // for Node.js Express back-end

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  // constructor(private token: TokenStorageService) { }

  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    let authReq = req;

    /*
    const token = this.token.getToken();
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token.access_token) });
    }
    return next.handle(authReq);
      */

    /*
    authReq = req.clone({ headers: req.headers.set('userId', 'Utente1') });
    authReq = req.clone({ headers: req.headers.set('Cache-Control', 'no-cache') });
    authReq = req.clone({ headers: req.headers.set('Pragma', 'no-cache') });

    authReq = req.clone({ headers: req.headers.set('userId', 'Utente1') });
*/
    const headers = new HttpHeaders({
      userId: 'Utente1',
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      Pragma: 'no-cache',
      Expires: '0',
    });

    // const cloneReq = req.clone({headers});
    authReq = req.clone({ headers });
    return next.handle(authReq);
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
];
