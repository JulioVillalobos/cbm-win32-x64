import { Injectable } from '@angular/core';
import { SessionHelper } from '../helpers/session/session.helper';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private sessionHelper: SessionHelper) { }

  isAuthenticated(): boolean {
    const user = this.sessionHelper.getItem('user');
    if (user || user != null) return true;
    return false;
  }
}
