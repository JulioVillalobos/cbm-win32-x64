/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core'; 
import { AppLayoutComponent } from './layout/app.layout.component';
import { AuthGuard } from './core/authentication/auth-guard';
import { NotfoundComponent } from './modules/notfound/notfound.component';

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'campus-bio-medico',
          component: AppLayoutComponent,
          children: [
            {
              path: '',
              loadChildren: () =>
                import('./modules/dashboard/dashboard.module').then(
                  m => m.DashboardModule
                ),
            },
          ],
          canActivate: [AuthGuard],
        },
        {
          path: 'auth',
          loadChildren: () =>
            import('./modules/auth/auth.module').then(
              m => m.AuthModule
            ),
        },
        { path: 'pages/notfound', component: NotfoundComponent },
        { path: '**', redirectTo: '/auth/login' },
        { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
      ],
      {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled',
        onSameUrlNavigation: 'reload',
      }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
