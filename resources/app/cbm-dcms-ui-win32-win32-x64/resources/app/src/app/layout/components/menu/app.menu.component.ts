import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from '../../service/app.layout.service';

@Component({
  selector: 'app-menu',
  templateUrl: './app.menu.component.html',
  styleUrls: ['./app.menu.component.scss'],
})
export class AppMenuComponent implements OnInit {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  model: any[] = [];

  constructor(public layoutService: LayoutService, private router: Router) {}

  ngOnInit(): void {
    this.model = [
      {
        items: [
          {
            label: 'Dashboard',
            icon: 'fa-solid fa-gauge',
            routerLink: ['/cerberus/dashboard'],
          },
          {
            label: 'News',
            icon: 'fa-regular fa-newspaper',
            items: [
              {
                label: 'Ansa',
                icon: 'fa-regular fa-newspaper',
                routerLink: ['/cerberus/news/ansa'],
              },
              {
                label: 'Roma Today',
                icon: 'fa-regular fa-newspaper',
                routerLink: ['/cerberus/news/roma-today'],
              },
              {
                label: 'Twitter',
                icon: 'fa-regular fa-newspaper',
                routerLink: ['/cerberus/news/twitter'],
              },
            ],
          },
          {
            label: 'Settings',
            icon: 'fa-solid fa-gear',
            routerLink: ['/cerberus/settings'],
          },
          // {
          //     label: 'Sources',
          //     icon: 'fa-solid fa-server',
          //     routerLink: ['/cerberus/sources'],
          // }
        ],
      },
    ];
  }
}
