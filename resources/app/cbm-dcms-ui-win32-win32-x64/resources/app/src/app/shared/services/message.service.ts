import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Message } from '../interfaces/message';

@Injectable()
export class MessageService {
  private messageSource = new Subject<Message | Message[]>();
  private clearSource = new Subject<string>();

  messageObserver = this.messageSource.asObservable();
  clearObserver = this.clearSource.asObservable();

  add(message: Message): void {
    if (message) {
      this.messageSource.next(message);
    }
  }

  addAll(messages: Message[]): void {
    if (messages && messages.length) {
      this.messageSource.next(messages);
    }
  }

  clear(key?: string): void {
    this.clearSource.next(key || null);
  }
}
