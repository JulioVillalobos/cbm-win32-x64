import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { SessionHelper } from 'src/app/core/helpers/session/session.helper';
import { User } from 'src/app/shared/interfaces/user.interface';
 

@Component({
  selector: 'app-login',
  styleUrls: ['./login.component.scss'],
  templateUrl: './login.component.html',
})
export class LoginComponent {
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
  });
  userMock: User = {
    username: 'lucky',
    password: 'password',
  };

  constructor(
    public messageService: MessageService,
    private router: Router,
    private sessionHelper: SessionHelper
  ) {
    this.sessionHelper.clearSession();
  }

  login(): void {
    const userCredential: Partial<User> = {
      ...this.loginForm.value,
    };
    if (
      userCredential.username === this.userMock.username &&
      userCredential.password === this.userMock.password
    ) {
      this.sessionHelper.saveItem<User>('user', this.userMock);
      this.router.navigateByUrl('/campus-bio-medico/dashboard/home');
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'Bad credentials!',
        detail: 'Username or password not valid',
      });
    }
  }
}
