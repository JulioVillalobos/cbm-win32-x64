import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 

import { HomeRoutingModule } from './home-routing.module'; 
import { HomeViewComponent } from './views/home-view/home-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CardModule } from 'primeng/card';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { CheckboxModule } from 'primeng/checkbox';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { PasswordModule } from 'primeng/password';
import { ProgressBarModule } from 'primeng/progressbar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SpeedDialModule } from 'primeng/speeddial';
import { TooltipModule } from 'primeng/tooltip';
import { RippleModule } from 'primeng/ripple';
import { DividerModule } from 'primeng/divider';
import { ScrollerModule } from 'primeng/scroller';
import { PanelModule } from 'primeng/panel';
import { MenuModule } from 'primeng/menu';
import { TabViewModule } from 'primeng/tabview';
import {TreeModule} from 'primeng/tree';
import { ListboxModule } from 'primeng/listbox';
import { MenubarModule } from 'primeng/menubar';

@NgModule({
  declarations: [
    HomeViewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    RouterModule,
    CardModule,
    DialogModule,
    ButtonModule,
    ToastModule,
    DropdownModule,
    InputTextModule,
    InputMaskModule,
    CheckboxModule, 
    FieldsetModule,
    InputTextareaModule,
    InputNumberModule,
    PasswordModule,
    ProgressBarModule,
    ScrollPanelModule,
    ConfirmDialogModule,
    SpeedDialModule,
    TooltipModule,
    RippleModule,
    DividerModule,
    ScrollerModule,
    PanelModule,
    MenuModule,
    TabViewModule,  
    TreeModule,
    ListboxModule,
    MenubarModule
  ]
})
export class HomeModule { }
