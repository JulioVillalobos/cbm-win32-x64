import { Component, HostListener, OnInit } from '@angular/core';
import { SelectItem, TreeNode } from 'primeng/api';
import * as $ from 'jquery';

import Hammer from "hammerjs";
import dicomParser from "dicom-parser";
import * as cornerstone from "cornerstone-core";
import cornerstoneMath from "cornerstone-math";
import * as cornerstoneWADOImageLoader from "cornerstone-wado-image-loader";
import * as cornerstoneTools from "cornerstone-tools";
import { FileServices } from 'src/app/shared/services/files.service';
import { TAG_DICT } from '../helpers/dataDictionary';
import { uids } from '../helpers/uids';


@Component({
    selector: 'app-home-view',
    templateUrl: './home-view.component.html',
    styleUrls: ['./home-view.component.scss'],
})
export class HomeViewComponent implements OnInit {

    constructor(private fileServices: FileServices) { }


    items: any;

    showMenu = false;



    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {

        let findClass = $(event.target).hasClass('p-treenode-content p-treenode-selectable')

        if (findClass) {
            if (event.key == 'ArrowDown' || event.key == 'ArrowUp') {
                const keydown = new KeyboardEvent('keydown', {
                    bubbles: true, cancelable: true, keyCode: 13
                });
                event.target.dispatchEvent(keydown);
                console.log("Enter")
            }
        }
    }


    ngOnInit(): void {

        this.items = [
            {
                label: 'Line',
                icon: 'pi pi-fw pi-minus',
                command: () => {
                    this.LengthTool();
                },
            },
            {
                label: 'Contrast',
                icon: 'pi pi-fw pi-sun',
                command: () => {
                    this.WwwcTool();
                },
            },
            {
                label: 'Move',
                icon: 'pi pi-fw pi-arrows-alt',
                command: () => {
                    this.ZoomAndPanTool();
                },
            }, {
                label: 'Touch',
                icon: 'pi pi-fw pi-arrows-alt',
                command: () => {
                    this.ZoomTouchPinchAndPanMultiTouchTool();
                },
            },
            {
                label: 'Clear',
                icon: 'pi pi-fw pi-times',
                command: () => {
                    this.clearTools();
                },
            },
        ];

        this.fileServices.getFiles$().subscribe(files => {
            this.uploadFiles(files)
        })

        this._initCornerstone();
    }

    _initCornerstone() {
        // Externals
        cornerstone.registerImageLoader("wadouri", this.transformDicom);
        cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
        cornerstoneWADOImageLoader.external.dicomParser = dicomParser;
        // Image Loader
        const config = {
            maxWebWorkers: navigator.hardwareConcurrency || 1,
            startWebWorkersOnDemand: true,
            taskConfiguration: {
                decodeTask: {
                    initializeCodecsOnStartup: false,
                    usePDFJS: false,
                },
            },
        };
        cornerstoneWADOImageLoader.webWorkerManager.initialize(config);


        cornerstoneTools.external.cornerstone = cornerstone;
        cornerstoneTools.external.Hammer = Hammer;
        cornerstoneTools.external.cornerstoneMath = cornerstoneMath;
        cornerstoneTools.init();
    }



    element = document.querySelector('#content');

    LengthTool() {
        //  cornerstoneTools.clearToolState(this.element, toolName);
        // LINEA METRICA 
        const LengthTool = cornerstoneTools.LengthTool;
        // Make sure we have at least one element Enabled
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        // Adds tool to ALL currently Enabled elements
        //       cornerstoneTools.addTool(LengthTool);
        cornerstoneTools.addTool(LengthTool);
        // Set Tool Active
        cornerstoneTools.setToolActive('Length', { mouseButtonMask: 1 });
        console.log("LengthTool")
    }


    WwwcTool() {
        // CONTRASTO
        const WwwcTool = cornerstoneTools.WwwcTool;
        // Make sure we have at least one element Enabled
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        // Adds tool to ALL currently Enabled elements
        //       cornerstoneTools.addTool(LengthTool);
        cornerstoneTools.addTool(WwwcTool);
        // Set Tool Active
        cornerstoneTools.setToolActive("Wwwc", { mouseButtonMask: 1 }); // Left & Touch
        console.log("WwwcTool")
    }

    ZoomAndPanTool() {
        // MOUSE INGRANDIMENTO
        const ZoomTool = cornerstoneTools.ZoomTool;
        const PanTool = cornerstoneTools.PanTool;
        const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
        // Make sure we have at least one element Enabled
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        // Adds tool to ALL currently Enabled elements
        //       cornerstoneTools.addTool(LengthTool);
        cornerstoneTools.addTool(ZoomTool);
        cornerstoneTools.addTool(PanTool);
        cornerstoneTools.addTool(ZoomMouseWheelTool);
        // Set Tool Active
        cornerstoneTools.setToolActive("Zoom", { mouseButtonMask: 1 }); // left
        cornerstoneTools.setToolActive("Pan", { mouseButtonMask: 2 }); // Right
        cornerstoneTools.setToolActive("ZoomMouseWheel", {});
        console.log("ZoomMouseWheelTool")
        console.log("ZoomAndPanTool")
    }


    ZoomTouchPinchAndPanMultiTouchTool() {
        // TOUCH INGRANDIMENTO
        const ZoomTouchPinchTool = cornerstoneTools.ZoomTouchPinchTool;
        const PanMultiTouchTool = cornerstoneTools.PanMultiTouchTool;

        // Make sure we have at least one element Enabled
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        // Adds tool to ALL currently Enabled elements
        //       cornerstoneTools.addTool(LengthTool);
        cornerstoneTools.addTool(ZoomTouchPinchTool);
        cornerstoneTools.addTool(PanMultiTouchTool);
        // Set Tool Active
        cornerstoneTools.setToolActive("ZoomTouchPinch", {});
        cornerstoneTools.setToolActive("PanMultiTouch", {});
        console.log("ZoomTouchPinchAndPanMultiTouchTool")
    }

    ZoomMouseWheelTool() {
        // RESET ZOOM
        const ZoomMouseWheelTool = cornerstoneTools.ZoomMouseWheelTool;
        // Make sure we have at least one element Enabled
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        // Adds tool to ALL currently Enabled elements
        //       cornerstoneTools.addTool(LengthTool);
        cornerstoneTools.addTool(ZoomMouseWheelTool);
        // Set Tool Active
        cornerstoneTools.setToolActive("ZoomMouseWheel", {});
        console.log("ZoomMouseWheelTool")
    }

    clearTools() {
        console.log(cornerstoneTools.getToolState(this.element, 'Length'));
        cornerstoneTools.clearToolState(this.element, 'Length');
        cornerstoneTools.clearToolState(this.element, 'Wwwc');
        cornerstoneTools.clearToolState(this.element, 'Zoom');
        cornerstoneTools.clearToolState(this.element, 'Pan');
        cornerstoneTools.clearToolState(this.element, 'ZoomTouchPinch');
        cornerstoneTools.clearToolState(this.element, 'PanMultiTouch');
        this.element = document.querySelector('#content');
        cornerstone.enable(this.element);
        console.log("clearTools")
    }

    cornerDicoms: any[] = [];
    namePackageFiles: string;

    uploadFiles(fileList: FileList) {
        let imageIds = [];
        if (fileList.length != 0) {
            this.namePackageFiles = fileList[0].webkitRelativePath.split("/")[0];
            for (let i = 0; i < fileList.length; i++) {
                let result = cornerstoneWADOImageLoader.wadouri.fileManager.add(fileList[i]);
                imageIds.push(result)
            }
            this.transformDicom(imageIds)
        }
    }


    listDicoms = [];

    transformDicom(imageIds: any[]) {

        let promiseAll = [];

        imageIds.forEach(element => {
            promiseAll.push(cornerstone.loadImage(element));
        });

        Promise.all(promiseAll).then((result) => {
            this.cornerDicoms = result;
        }).catch((e) => console.log("ERROR : transformDicom", e)).finally(() => {
            console.log('completed');

            let orderDicoms = this.orderByInstanceNumber(this.cornerDicoms);
            this.listDicoms = [...this.listDicoms, ...orderDicoms];

            let folder: TreeNode = {};
            folder = this.loadTreeDicoms(orderDicoms);
            this.folders.push(folder);
        });


    }

    orderByInstanceNumber(dicoms: any[]) {
        dicoms.map(dicom => {
            let dataSet = dicom.data;


            let listProperty = [{ name: 'instanceNumber', value: 'x00200013' }, { name: 'SOPInstanceUID', value: 'x00080018' }];

            listProperty.forEach(item => {
                let str = dataSet.string(item.value);
                let stringIsAscii = this.isASCII(str);
                if (stringIsAscii) {
                    if (str !== undefined) {
                        //   console.log("escapeSpecialCharacters", this.escapeSpecialCharacters(str));
                        dicom[item.name] = this.escapeSpecialCharacters(str);
                    }
                }

            });

            return dicom;
        });

        let dicomsOrderByInstanceNumber = dicoms.sort(({ instanceNumber: a }, { instanceNumber: b }) => a - b);
        return dicomsOrderByInstanceNumber;
    }

    selectedFile: TreeNode;
    folders: TreeNode[] = [];

    loadTreeDicoms(dicoms: any[]) {
        let children = [];
        let result: TreeNode = {};

        let id = Math.floor(Math.random() * 100000).toString();

        dicoms.forEach((dicom, index) => {
            let child: TreeNode = {
                key: id + "-" + index,
                label: "Dicom " + dicom.instanceNumber,
                data: dicom.SOPInstanceUID,
                expanded: true,
                icon: "pi pi-image"
            }
            children.push(child)
        })
        result = {
            key: id,
            label: this.namePackageFiles,
            data: "package",
            expanded: true,
            expandedIcon: "pi pi-folder-open",
            collapsedIcon: "pi pi-folder",
            children: children,
        }
        return result;
    }


    metaData: any;

    instanceNumber = "";

    nodeSelect(event) {
        let data = event.node;

        if (data.parent != undefined) {
            let image = this.listDicoms.find(x => x.SOPInstanceUID === event.node.data);
            this.instanceNumber = image.instanceNumber;
            this.loadViewDicom(image);
            this.metaData = this.getMetaData(image.data);
            this.viewDetails(this.metaData);
            this.showMenu = true; 
        }

    }


    viewMetaData = [];
    selectedList: SelectItem = { value: '' };

    viewDetails(data: any) {
        this.viewMetaData = [];
        data[0].forEach(element => {
            if (element.value) {
                this.viewMetaData.push({ name: element.name, value: element.value });
            }
        });
    }


   // widthContent = "0";
   // heightContent = "0";
    loadViewDicom(image: any) {
        $('#content').width("685px");
        $('#content').height("685px");
        this.element = $('#content').get(0); 
        cornerstone.enable(this.element);
        cornerstone.displayImage(this.element, image);
    }


    getMetaData(dataSet: any) {
        let dataOutput = [];

        let keys = [];
        keys = this.getKeys(dataSet);

        let result = [];
        result = this.getResults(dataSet, keys);

        dataOutput.push(result);
        return dataOutput;
    }

    getKeys(dataSet: any) {
        let keys = [];
        Object.keys(dataSet.elements).forEach(propertyName => {
            keys.push(propertyName);
        });
        return keys;
    }


    getResults(dataSet: any, keys: any) {
        let result: any[] = [];

        keys.forEach((key, index) => {

            let propertyName = keys[index];

            let element = dataSet.elements[propertyName];

            let tag = this.getTag(element.tag);
            let tempTag = tag != undefined ? tag.name : "Item " + element.tag;

            if (element.items) {

                element.items.forEach(element => {
                    let dataFormat = { key: key, name: tempTag, items: this.getMetaData(element.dataSet) }
                    if (result[index]) {
                        result[index].push(dataFormat);

                    } else {
                        result[index] = [];
                        result[index].push(dataFormat);
                    }

                });


            } else {

                let str = dataSet.string(propertyName);

                let stringIsAscii = this.isASCII(str);

                if (stringIsAscii) {

                    if (str !== undefined) {
                        let value = this.escapeSpecialCharacters(str) + this.mapUid(str);
                        result.push({ key: key, name: tempTag, value: value })
                    } else {
                        result.push({ key: key, name: tempTag, description: 'none' })
                    }
                }

            }

        });

        return result;

    }


    getTag(tag) {
        let group = tag.substring(1, 5);
        let element = tag.substring(5, 9);
        let tagIndex = ("(" + group + "," + element + ")").toUpperCase();
        let attr = TAG_DICT[tagIndex];
        return attr;
    }


    isASCII(str) {
        return /^[\x00-\x7F]*$/.test(str);
    }

    escapeSpecialCharacters(str) {
        return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
    }

    mapUid(str) {
        let uid = uids[str];
        if (uid) {
            return ' [ ' + uid + ' ]';
        }
        return '';
    }


    /*

    
    dataSet: any;
    untilTag = "";
    showP10Header = false;
    showPrivateElements = false;
    showEmptyValues = false;

    rusha = Rusha.createHash();


    dumpDataSet(dataSet, output) {

        let maxLength = 128;
        let untilTag = "";
        let showPrivateElements = false;
        let showP10Header = false;
        let showEmptyValues = false;
        let showLength = false;
        let showVR = false;
        let showGroupElement = false;
        let showFragments = false;
        let showFrames = false;
        let showSHA1 = false;
        let file = undefined;
        let sha1Hash = "";


        function getTag(tag) {
            let group = tag.substring(1, 5);
            let element = tag.substring(5, 9);
            let tagIndex = ("(" + group + "," + element + ")").toUpperCase();
            let attr = TAG_DICT[tagIndex];
            return attr;
        }


        try {
            let keys = [];
            for (let propertyName in dataSet.elements) {
                keys.push(propertyName);
            }
            keys.sort();

            for (let k = 0; k < keys.length; k++) {

                let propertyName = keys[k];
                let element = dataSet.elements[propertyName];


                if (showP10Header === false && element.tag <= "x0002ffff") {
                    continue;
                }
                if (showPrivateElements === false && dicomParser.isPrivateTag(element.tag)) {
                    continue;
                }
                if (showEmptyValues === false && element.length <= 0) {
                    continue;
                }

                let text = "";
                let title = "";

                let color = '#ffff';

                let tag = getTag(element.tag);

                try {
                    if (tag === undefined) {
                        text += element.tag;
                        text += " : ";

                        let lengthText = "length=" + element.length;
                        if (element.hadUndefinedLength) {
                            lengthText += " (-1)";
                        }

                        if (showLength) {
                            text += lengthText + "; ";
                        }

                        title += lengthText;

                        let vrText = "";
                        if (element.vr) {
                            vrText += "VR=" + element.vr;
                        }


                        if (showVR) {
                            text += vrText + "; ";
                        }

                        if (vrText) {
                            title += "; " + vrText;
                        }

                        title += "; dataOffset=" + element.dataOffset;
                        color = '#C8C8C8';
                    }
                    else {
                        text += tag.name;

                        if (showGroupElement) {
                            text += "(" + element.tag + ")";
                        }

                        text += " : ";

                        title += "(" + element.tag + ")";

                        let lengthText = " length=" + element.length;
                        if (element.hadUndefinedLength) {
                            lengthText += " (-1)";
                        }


                        if (showLength) {
                            text += lengthText + "; ";
                        }

                        title += "; " + lengthText;

                        let vrText = "";
                        if (element.vr) {
                            vrText += "VR=" + element.vr;
                        }

                        if (showVR) {
                            text += vrText + "; ";
                        }

                        if (vrText) {
                            title += "; " + vrText;
                        }

                        title += "; dataOffset=" + element.dataOffset;

                    }

                } catch (e) {
                    console.log("catch 1", e)
                }



                try {
                    if (element.items) {
                        output.push('<li>' + text + '</li>');
                        output.push('<ul>');


                        let itemNumber = 0;
                        element.items.forEach(function (item) {
                            output.push('<li>Item #' + itemNumber++ + ' ' + item.tag);
                            let lengthText = " length=" + item.length;
                            if (item.hadUndefinedLength) {
                                lengthText += " (-1)";
                            }

                            if (showLength === true) {
                                text += lengthText + "; ";
                                output.push(lengthText);
                            }

                            output.push('</li>');
                            output.push('<ul>');
                            this.dumpDataSet(item.dataSet, output);
                            output.push('</ul>');
                        });
                        output.push('</ul>');
                    }

                    else if (element.fragments) {
                        text += "encapsulated pixel data with " + element.basicOffsetTable.length + " offsets and " +
                            element.fragments.length + " fragments";
                        //  text += this.sha1Text(dataSet.byteArray, element.dataOffset, element.length);

                        output.push("<li title='" + title + "'=>" + text + '</li>');

                        if (showFragments && element.encapsulatedPixelData) {
                            output.push('Fragments:<br>');
                            output.push('<ul>');
                            let itemNumber = 0;
                            element.fragments.forEach(function (fragment) {
                                let str = '<li>Fragment #' + itemNumber++ + ' dataOffset = ' + fragment.position;
                                str += '; offset = ' + fragment.offset;
                                str += '; length = ' + fragment.length;
                                str += this.sha1Text(dataSet.byteArray, fragment.position, fragment.length);
                                str += '</li>';

                                output.push(str);
                            });
                            output.push('</ul>');
                        }
                        if (showFrames && element.encapsulatedPixelData) {
                            output.push('Frames:<br>');
                            output.push('<ul>');
                            let bot = element.basicOffsetTable;

                            if (bot.length === 0) {
                                bot = dicomParser.createJPEGBasicOffsetTable(dataSet, element);
                            }

                            function imageFrameLink(frameIndex) {
                                let linkText = "<a class='imageFrameDownload' ";
                                linkText += "data-frameIndex='" + frameIndex + "'";
                                linkText += " href='#'> Frame #" + frameIndex + "</a>";
                                return linkText;
                            }

                            for (let frameIndex = 0; frameIndex < bot.length; frameIndex++) {
                                let str = "<li>";
                                // str += imageFrameLink(frameIndex, "Frame #" + frameIndex);
                                str += imageFrameLink("Frame #" + frameIndex);
                                str += ' dataOffset = ' + (element.fragments[0].position + bot[frameIndex]);
                                str += '; offset = ' + (bot[frameIndex]);
                                let imageFrame = dicomParser.readEncapsulatedImageFrame(dataSet, element, frameIndex, bot);
                                str += '; length = ' + imageFrame.length;
                                //    str += this.sha1Text(imageFrame);
                                str += '</li>';
                                output.push(str);
                            }
                            output.push('</ul>');
                        }
                    }

                    else {

                        let vr;
                        if (element.vr !== undefined) {
                            vr = element.vr;
                        }
                        else {
                            if (tag !== undefined) {
                                vr = tag.vr;
                            }
                        }


                        if (element.length < 128) {

                            if (element.vr === undefined && tag === undefined) {
                                if (element.length === 2) {
                                    text += " (" + dataSet.uint16(propertyName) + ")";
                                }
                                else if (element.length === 4) {
                                    text += " (" + dataSet.uint32(propertyName) + ")";
                                }

                                let str = dataSet.string(propertyName);
                                let stringIsAscii = this.isASCII(str);

                                if (stringIsAscii) {

                                    if (str !== undefined) {
                                        text += '"' + this.escapeSpecialCharacters(str) + '"' + this.mapUid(str);
                                    }
                                }
                                else {
                                    if (element.length !== 2 && element.length !== 4) {
                                        color = '#C8C8C8';

                                        text += "<i>binary data</i>";
                                    }
                                }
                            }
                            else {
                                function isStringVr(vr) {
                                    if (vr === 'AT'
                                        || vr === 'FL'
                                        || vr === 'FD'
                                        || vr === 'OB'
                                        || vr === 'OF'
                                        || vr === 'OW'
                                        || vr === 'SI'
                                        || vr === 'SQ'
                                        || vr === 'SS'
                                        || vr === 'UL'
                                        || vr === 'US'
                                    ) {
                                        return false;
                                    }
                                    return true;
                                }
                                if (isStringVr(vr)) {

                                    let str = dataSet.string(propertyName);
                                    let stringIsAscii = this.isASCII(str);

                                    if (stringIsAscii) {

                                        if (str !== undefined) {
                                            text += '"' + this.escapeSpecialCharacters(str) + '"' + this.mapUid(str);
                                        }
                                    }
                                    else {
                                        if (element.length !== 2 && element.length !== 4) {
                                            color = '#C8C8C8';

                                            text += "<i>binary data</i>";
                                        }
                                    }
                                }
                                else if (vr === 'US') {
                                    text += dataSet.uint16(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 2; i++) {
                                        text += '\\' + dataSet.uint16(propertyName, i);
                                    }
                                }
                                else if (vr === 'SS') {
                                    text += dataSet.int16(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 2; i++) {
                                        text += '\\' + dataSet.int16(propertyName, i);
                                    }
                                }
                                else if (vr === 'UL') {
                                    text += dataSet.uint32(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 4; i++) {
                                        text += '\\' + dataSet.uint32(propertyName, i);
                                    }
                                }
                                else if (vr === 'SL') {
                                    text += dataSet.int32(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 4; i++) {
                                        text += '\\' + dataSet.int32(propertyName, i);
                                    }
                                }
                                else if (vr == 'FD') {
                                    text += dataSet.double(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 8; i++) {
                                        text += '\\' + dataSet.double(propertyName, i);
                                    }
                                }
                                else if (vr == 'FL') {
                                    text += dataSet.float(propertyName);
                                    for (let i = 1; i < dataSet.elements[propertyName].length / 4; i++) {
                                        text += '\\' + dataSet.float(propertyName, i);
                                    }
                                }
                                else if (vr === 'OB' || vr === 'OW' || vr === 'UN' || vr === 'OF' || vr === 'UT') {
                                    color = '#C8C8C8';

                                    if (element.length === 2) {
                                        text += "<i>" + this.dataDownloadLink(element, "binary data") + " of length " + element.length + " as uint16: " + dataSet.uint16(propertyName) + "</i>";
                                    } else if (element.length === 4) {
                                        text += "<i>" + this.dataDownloadLink(element, "binary data") + " of length " + element.length + " as uint32: " + dataSet.uint32(propertyName) + "</i>";
                                    } else {
                                        text += "<i>" + this.dataDownloadLink(element, "binary data") + " of length " + element.length + " and VR " + vr + "</i>";
                                    }
                                }
                                else if (vr === 'AT') {
                                    let group = dataSet.uint16(propertyName, 0);
                                    let groupHexStr = ("0000" + group.toString(16)).substr(-4);
                                    let element = dataSet.uint16(propertyName, 1);
                                    let elementHexStr = ("0000" + element.toString(16)).substr(-4);
                                    text += "x" + groupHexStr + elementHexStr;
                                }
                                else if (vr === 'SQ') {
                                }
                                else {
                                    text += "<i>no display code for VR " + vr + " yet, sorry!</i>";
                                }
                            }

                            if (element.length === 0) {
                                color = '#C8C8C8';
                            }
                        }
                        else {
                            color = '#C8C8C8';


                            text += "<i>" + this.dataDownloadLink(element, "data");
                            text += " of length " + element.length + " for VR " + vr + " too long to show</i>";
                            //   text += this.sha1Text(dataSet.byteArray, element.dataOffset, element.length);
                        }

                        output.push('<li style="color:' + color + '; " title="' + title + '">' + text + '</li>');

                    }

                } catch (e) {
                    console.log("catch 2", e)
                }


            }
        } catch (err) {
            let ex = {
                exception: err,
                output: output
            }
            throw ex;
        }
    }



    dataDownloadLink(element, text) {
        let linkText = "<a class='dataDownload' href='#' data-tag='" + element.tag + "'";
        linkText += " data-dataOffset='" + element.dataOffset + "'";
        linkText += " data-length='" + element.length + "'";
        linkText += ">" + text + "</a>";
        return linkText;
    }

  
     sha1(byteArray, position, length) {
      position = position || 0;
      length = length || byteArray.length;
      var subArray = dicomParser.sharedCopy(byteArray, position, length);
      return this.rusha.digest(subArray);
    }
    
     sha1Text(byteArray, position, length) {
     
      var text = "; SHA1 " + this.sha1(byteArray, position, length);
      return text;
    }
     */




}



