import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(private readonly auth: AuthService) { }

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      const basePath = window.origin;
      if (environment.production) window.location.href = basePath + "/argo/";
      else window.location.href = basePath;
      return false;
    }
    return true;
  }
}
