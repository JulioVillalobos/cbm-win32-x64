import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ScrapingStatus } from 'src/app/shared/interfaces';
import { environment } from 'src/environments/environment';

const API_URL = `${environment.host}/api/v1/it`;

@Injectable({
  providedIn: 'root'
})
export class ManagementApiService {

  constructor(private http: HttpClient) { }

  public getScrapingStatus(): Observable<ScrapingStatus> {
    return this.http.get<ScrapingStatus>(`${API_URL}/scrapingStatus`);
  }
}
