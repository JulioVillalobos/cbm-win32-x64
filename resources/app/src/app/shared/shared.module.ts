import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';
import { MenuModule } from 'primeng/menu';
import { TabViewModule } from 'primeng/tabview';
import { PanelModule } from 'primeng/panel';
import { DividerModule } from 'primeng/divider';
import { TooltipModule } from 'primeng/tooltip';
import { SpeedDialModule } from 'primeng/speeddial';
import { ScrollerModule } from 'primeng/scroller';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { InputMaskModule } from 'primeng/inputmask';
import { FieldsetModule } from 'primeng/fieldset';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputNumberModule } from 'primeng/inputnumber';
import { PasswordModule } from 'primeng/password';
import { ProgressBarModule } from 'primeng/progressbar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { CalendarModule } from 'primeng/calendar';
import { AlertModalComponent } from './components/alert-modal/alert-modal.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { CountUpDirective } from './directives/count-up.directive';

const MODULE_COMPONENTS = [AlertModalComponent, CountUpDirective];

@NgModule({
    declarations: MODULE_COMPONENTS,
    imports: [
        CommonModule,
        CardModule,
        CommonModule,
        ReactiveFormsModule,
        ConfirmDialogModule,
        RouterModule,
        CardModule,
        DialogModule,
        ButtonModule,
        ToastModule,
        FormsModule,
        DropdownModule,
        InputTextModule,
        InputMaskModule,
        CheckboxModule,
        FieldsetModule,
        InputTextareaModule,
        InputNumberModule,
        PasswordModule,
        ProgressBarModule,
        ScrollPanelModule,
        DynamicDialogModule,
        SpeedDialModule,
        TooltipModule,
        DividerModule,
        ScrollerModule,
        PanelModule,
        MenuModule,
        TabViewModule,
        ChartModule,
        CalendarModule,
    ],
    exports: MODULE_COMPONENTS,
    entryComponents: MODULE_COMPONENTS,
})
export class SharedModule {}
