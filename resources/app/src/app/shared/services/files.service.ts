import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileServices {

  private loading$ = new BehaviorSubject<boolean>(false);

  private files$ = new BehaviorSubject<any>([]);

  getLoading$(): Observable<boolean> {
    return this.loading$.asObservable();
  }

  setLoading(isLoading: boolean): void {
    this.loading$.next(isLoading);
  }

  getFiles$(): Observable<any> {
    return this.files$.asObservable();
  }

  setFiles$(files: any): void {
    this.files$.next(files);
  }
}
