import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}

  setLocalStorage(name: string, value: string): void {
    localStorage.setItem(name, value);
  }

  getLocalStorage(name: string): string {
    return localStorage.getItem(name);
  }

  removeLocalStorage(name: string): void {
    localStorage.removeItem(name);
  }
}
