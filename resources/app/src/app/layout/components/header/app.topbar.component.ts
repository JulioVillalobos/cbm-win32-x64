import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from '../../service/app.layout.service';
import { Router } from '@angular/router';
import { FileServices } from 'src/app/shared/services/files.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html',
})
export class AppTopBarComponent implements OnInit {
  items!: MenuItem[];

  @ViewChild('menubutton') menuButton!: ElementRef;

  @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

  @ViewChild('topbarmenu') menu!: ElementRef;

  constructor(public layoutService: LayoutService, private router: Router, private fileServices: FileServices) { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Settings',
        icon: 'pi pi-fw pi-cog',
        items: [
          /*
                    {
                      label: 'Authorized licenses',
                      icon: 'pi pi-fw pi-key',
                      command: (): void => {
                        this.router.navigateByUrl('/cerberus/settings/licenses');
                      },
                    },
                    */
        ],
      },
      {
        label: 'Lucky',
        icon: 'pi pi-fw pi-user',
        items: [
          {
            label: 'Profile',
            icon: 'pi pi-fw pi-user-edit',
          },
        ],
      },
      {
        label: 'Quit',
        icon: 'pi pi-fw pi-power-off',
        command: (): void => {
          this.router.navigateByUrl('/auth/login');
        },
      },
    ];
  }


  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  setFiles(fileList: FileList) {
    console.log("fileList", fileList)
    this.fileServices.setFiles$(fileList);
    this.fileInput.nativeElement.value = "";


  }

}
