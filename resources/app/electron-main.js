const { app, BrowserWindow } = require('electron')

const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    icon: __dirname + '/src/assets/images/favicon/android-icon-192x192.png',
  })

  win.loadFile('./dist/campus-biomedico/index.html')
}

app.whenReady().then(() => {
  createWindow()
})